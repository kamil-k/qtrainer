#include "database.h"

Database::Database(){
}

void Database::init(){


    QFile file(DATABASE_FILENAME);
    bool databaseExists;
    if(!file.exists()){
        qDebug() << "Database not exists";
        databaseExists = false;
    }
    else{
        qDebug() << "Database exists";
        databaseExists = true;
    }
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(DATABASE_FILENAME);
    bool ok = db.open();
    if(ok){
        qDebug() << "Database opened succesfully";
    }
    else{
        qDebug() << "Database during open database";
    }
    if(!databaseExists){
        databaseSetup();
    }
}

void Database::databaseSetup(){
    qDebug() << "Creating database schema";
    db.exec(" CREATE TABLE [Question] (   \
            [QUESTION_ID] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, \
            [QUESTION_CONTENT] TEXT  NOT NULL, \
            [CORRECT_ANSWERS] INTEGER  NOT NULL, \
            [CATEGORY_ID] INTEGER  NULL, \
            [CERTIFICATE_ID] INTEGER NULL, \
            [EXPLANATION] TEXT  NULL \
            ); ");

    db.exec("CREATE TABLE [Answer] ( \
            [ANSWER_ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, \
            [QUESTION_ID] INTEGER  NOT NULL, \
            [ANSWER_CONTENT] TEXT  NOT NULL, \
            [CORRECT] BOOLEAN  NOT NULL, \
            FOREIGN KEY(QUESTION_ID) REFERENCES Question(QUESTION_ID) \
            );");

    db.exec("CREATE TABLE [Certificate] ( \
            [CERTIFICATE_ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, \
            [NAME] TEXT NOT NULL \
            );");

    db.exec("CREATE TABLE [Category] ( \
            [CATEGORY_ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT, \
            [NAME] TEXT NOT NULL \
            );");
}

Question Database::getQuestionById(int id){
    Question question;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Question WHERE QUESTION_ID = ?" );
    query.bindValue( 0, QVariant(id) );
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    if( query.next() ){
        question.questionId = query.value("QUESTION_ID").toInt();
        question.questionContent = query.value("QUESTION_CONTENT").toString();
        question.correctAnswers = query.value("CORRECT_ANSWERS").toInt();
        question.category = query.value("CATEGORY_ID").toInt();
        question.certType = query.value("CERTIFICATE_ID").toInt();
        question.explanation = query.value("EXPLANATION").toString();
        QSqlQuery answerQuery(db);
        answerQuery.prepare( "SELECT * FROM Answer WHERE QUESTION_ID = ?" );
        answerQuery.bindValue( 0, QVariant(id) );
        if( !answerQuery.exec() )
        {
            qDebug() << "Query execute error: " << query.lastError();
        }
        while( answerQuery.next() ){
            Answer* answer = new Answer();
            answer->answerId = answerQuery.value("ANSWER_ID").toInt();
            answer->answerContent = answerQuery.value("ANSWER_CONTENT").toString();
            answer->correct = (answerQuery.value("CORRECT").toString()=="Y") ? true : false;
            question.answers.append(answer);
        }

    }
    return question;
}

QList<Question*> Database::getRandomQuestions(int amount){
    QList<Question*> questions;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Question ORDER BY RANDOM() LIMIT ?" );
    query.bindValue( 0, QVariant(amount) );
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    while( query.next() ){
        //qDebug() << "Loading quesiton number: " << query.value("QUESTION_ID").toInt();
        Question* question = new Question();
        question->questionId = query.value("QUESTION_ID").toInt();
        question->questionContent = query.value("QUESTION_CONTENT").toString();
        question->correctAnswers = query.value("CORRECT_ANSWERS").toInt();
        question->category = query.value("CATEGORY_ID").toInt();
        question->certType = query.value("CERTIFICATE_ID").toInt();
        question->explanation = query.value("EXPLANATION").toString();
        QSqlQuery answerQuery(db);
        answerQuery.prepare( "SELECT * FROM Answer WHERE QUESTION_ID = ? ORDER BY RANDOM()" );
        answerQuery.bindValue( 0, QVariant(question->questionId) );
        if( !answerQuery.exec() )
        {
            qDebug() << "Query execute error: " << query.lastError();
        }
        while( answerQuery.next() ){
            Answer* answer = new Answer();
            answer->answerId = answerQuery.value("ANSWER_ID").toInt();
            answer->answerContent = answerQuery.value("ANSWER_CONTENT").toString();
            answer->correct = (answerQuery.value("CORRECT").toString()=="Y") ? true : false;
            question->answers.append(answer);
        }
        questions.append(question);
    }
    return questions;
}

QList<Question*> Database::getRandomQuestionsByCertificateId(int amount, int certificateId){
    QList<Question*> questions;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Question WHERE CERTIFICATE_ID = :certId ORDER BY RANDOM() LIMIT :amount" );
    query.bindValue( ":amount", QVariant(amount) );
    query.bindValue(":certId", QVariant(certificateId));
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    while( query.next() ){
        //qDebug() << "Loading quesiton number: " << query.value("QUESTION_ID").toInt();
        Question* question = new Question();
        question->questionId = query.value("QUESTION_ID").toInt();
        question->questionContent = query.value("QUESTION_CONTENT").toString();
        question->correctAnswers = query.value("CORRECT_ANSWERS").toInt();
        question->category = query.value("CATEGORY_ID").toInt();
        question->certType = query.value("CERTIFICATE_ID").toInt();
        question->explanation = query.value("EXPLANATION").toString();
        QSqlQuery answerQuery(db);
        answerQuery.prepare( "SELECT * FROM Answer WHERE QUESTION_ID = ? ORDER BY RANDOM()" );
        answerQuery.bindValue( 0, QVariant(question->questionId) );
        if( !answerQuery.exec() )
        {
            qDebug() << "Query execute error: " << query.lastError();
        }
        while( answerQuery.next() ){
            Answer* answer = new Answer();
            answer->answerId = answerQuery.value("ANSWER_ID").toInt();
            answer->answerContent = answerQuery.value("ANSWER_CONTENT").toString();
            answer->correct = (answerQuery.value("CORRECT").toString()=="Y") ? true : false;
            question->answers.append(answer);
        }
        questions.append(question);
    }
    return questions;
}

QList<Question*> Database::getAllQuestions(){
    QList<Question*> questions;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Question" );
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    while( query.next() ){
        //qDebug() << "Loading quesiton number: " << query.value("QUESTION_ID").toInt();
        Question* question = new Question();
        question->questionId = query.value("QUESTION_ID").toInt();
        question->questionContent = query.value("QUESTION_CONTENT").toString();
        question->correctAnswers = query.value("CORRECT_ANSWERS").toInt();
        question->category = query.value("CATEGORY_ID").toInt();
        question->certType = query.value("CERTIFICATE_ID").toInt();
        question->explanation = query.value("EXPLANATION").toString();
        QSqlQuery answerQuery(db);
        answerQuery.prepare( "SELECT * FROM Answer WHERE QUESTION_ID = ? ORDER BY RANDOM()" );
        answerQuery.bindValue( 0, QVariant(question->questionId) );
        if( !answerQuery.exec() )
        {
            qDebug() << "Query execute error: " << query.lastError();
        }
        while( answerQuery.next() ){
            Answer* answer = new Answer();
            answer->answerId = answerQuery.value("ANSWER_ID").toInt();
            answer->answerContent = answerQuery.value("ANSWER_CONTENT").toString();
            answer->correct = (answerQuery.value("CORRECT").toString()=="Y") ? true : false;
            question->answers.append(answer);
        }
        questions.append(question);
    }
    return questions;
}

QList<int> Database::getAnswerIdsByQuestion(int id){
    QList<int> answerIds;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Answer WHERE QUESTION_ID = ? ORDER BY ANSWER_ID" );
    query.bindValue( 0, QVariant(id) );
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    while( query.next() ){
        answerIds.append(query.value("ANSWER_ID").toInt());
    }
    return answerIds;
}

void Database::addQuestion(Question question){
    QSqlQuery query(db);
    if(question.questionId==0){
        query.prepare("INSERT INTO Question (QUESTION_CONTENT, CORRECT_ANSWERS, EXPLANATION, CERTIFICATE_ID, CATEGORY_ID) VALUES ( :queCont, :corAnsw, :expl, :crType, :cat )");
    }
    else{
       query.prepare("INSERT INTO Question (QUESTION_ID, QUESTION_CONTENT, CORRECT_ANSWERS, EXPLANATION, CERTIFICATE_ID, CATEGORY_ID) VALUES ( :id, :queCont, :corAnsw, :expl, :crType, :cat )");
       query.bindValue("id",question.questionId);
    }
    query.bindValue(":corAnsw",QVariant(question.correctAnswers));
    query.bindValue(":expl",QVariant(question.explanation));
    query.bindValue(":queCont",QVariant(question.questionContent));
    if(question.certType==0){
        query.bindValue(":crType", QVariant(QString()));
    }
    else{
        query.bindValue(":crType", QVariant(question.certType));
    }
    if(question.category==0){
        query.bindValue(":cat",QVariant(QString()));
    }
    else{
        query.bindValue(":cat",QVariant(question.category));
    }
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    int questionId = query.lastInsertId().toInt();
    foreach(Answer* answer, question.answers){
        QSqlQuery queryAnswer(db);
        queryAnswer.prepare("INSERT INTO Answer (QUESTION_ID, ANSWER_CONTENT, CORRECT) VALUES ( :quesId, :ansCont, :corr )");
        queryAnswer.bindValue(":quesId",QVariant(questionId));
        queryAnswer.bindValue(":ansCont",QVariant(answer->answerContent));
        QString correct = (answer->correct) ? "Y" : "N";
        queryAnswer.bindValue(":corr",QVariant(correct));
        if( !queryAnswer.exec() )
        {
            qDebug() << "Query execute error: " << queryAnswer.lastError();
        }
    }
}

int Database::getCertificateIdByName(QString name){
    QSqlQuery query(db);
    int id = 0;
    query.prepare("SELECT * FROM Certificate WHERE NAME = :name");
    query.bindValue( ":name", QVariant(name) );
    if( !query.exec() ){
        qDebug() << "Query execute error: " << query.lastError();
    }
    if(query.next()){
        id = query.value("CERTIFICATE_ID").toInt();
    }
    else{
        QSqlQuery insertQuery(db);
        insertQuery.prepare("INSERT INTO Certificate (NAME) VALUES ( :name )");
        insertQuery.bindValue( ":name", QVariant(name) );
        if( !insertQuery.exec() ){
            qDebug() << "Query execute error: " << insertQuery.lastError();
        }
        id = insertQuery.lastInsertId().toInt();
    }
    return id;
}

int Database::getCategoryIdByName(QString name){
    QSqlQuery query(db);
    int id = 0;
    query.prepare("SELECT * FROM Category WHERE NAME = :name");
    query.bindValue( ":name", QVariant(name) );
    if( !query.exec() ){
        qDebug() << "Query execute error: " << query.lastError();
    }
    if(query.next()){
        id = query.value("CATEGORY_ID").toInt();
    }
    else{
        QSqlQuery insertQuery(db);
        insertQuery.prepare("INSERT INTO Category (NAME) VALUES ( :name )");
        insertQuery.bindValue( ":name", QVariant(name) );
        if( !insertQuery.exec() ){
            qDebug() << "Query execute error: " << insertQuery.lastError();
        }
        id = insertQuery.lastInsertId().toInt();
    }
    return id;
}

QList<Category> Database::getAllCategories(){
    QList<Category> categories;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Category" );
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    while( query.next() ){
        Category category;
        category.categoryId = query.value("CATEGORY_ID").toInt();
        category.name = query.value("NAME").toString();
        categories.append(category);
    }
    return categories;
}

QList<Certificate> Database::getAllCertificates(){
    QList<Certificate> certificates;
    QSqlQuery query(db);
    query.prepare( "SELECT * FROM Certificate" );
    if( !query.exec() )
    {
        qDebug() << "Query execute error: " << query.lastError();
    }
    while( query.next() ){
        Certificate certificate;
        certificate.certificateId = query.value("CERTIFICATE_ID").toInt();
        certificate.name = query.value("NAME").toString();
        certificates.append(certificate);
    }
    return certificates;
}

QStringList Database::toStringList(QList<Certificate> certificates){
    QStringList certificateNames;
    foreach(Certificate certificate, certificates){
        certificateNames.append(certificate.name);
    }
    return certificateNames;
}

QStringList Database::toStringList(QList<Category> categories){
    QStringList categoryNames;
    foreach(Category category, categories){
        categoryNames.append(category.name);
    }
    return categoryNames;
}

QString Database::getCertificateNameById(int id){
    QSqlQuery query(db);
    QString name = "";
    query.prepare("SELECT * FROM Certificate WHERE CERTIFICATE_ID = :id");
    query.bindValue( ":id", QVariant(id) );
    if( !query.exec() ){
        qDebug() << "Query execute error: " << query.lastError();
    }
    if(query.next()){
        name = query.value("NAME").toString();
    }
    return name;
}

QString Database::getCategoryNameById(int id){
    QSqlQuery query(db);
    QString name = "";
    query.prepare("SELECT * FROM Category WHERE CATEGORY_ID = :id");
    query.bindValue( ":id", QVariant(id) );
    if( !query.exec() ){
        qDebug() << "Query execute error: " << query.lastError();
    }
    if(query.next()){
        name = query.value("NAME").toString();
    }
    return name;
}

void Database::deleteQuestionById(int id){
    QSqlQuery queryQuestion(db);
    queryQuestion.prepare("DELETE FROM Question WHERE QUESTION_ID = :id");
    queryQuestion.bindValue( ":id", QVariant(id) );
    if( !queryQuestion.exec() ){
        qDebug() << "Query execute error: " << queryQuestion.lastError();
    }
    QSqlQuery queryAnswer(db);
    queryAnswer.prepare("DELETE FROM Answer WHERE QUESTION_ID = :id");
    if( !queryAnswer.exec() ){
        qDebug() << "Query execute error: " << queryAnswer.lastError();
    }
    return;
}

int Database::amountOfQuestionsByCertificateId(int certificateId){
    QSqlQuery query(db);
    query.prepare("SELECT COUNT(*) FROM Question WHERE CERTIFICATE_ID = :id");
    query.bindValue( ":id", QVariant(certificateId) );
    if( !query.exec() ){
        qDebug() << "Query execute error: " << query.lastError();
    }
    query.next();
    return query.value(0).toInt();
}
