#ifndef ANSWER_H
#define ANSWER_H

#include <QString>

class Answer
{
public:
    Answer();
    Answer(int answerId, QString questionContent, bool correct);
    int answerId;
    QString answerContent;
    bool correct;
};

#endif // ANSWER_H
