#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QCheckBox>
#include <QLayoutItem>
#include <QVBoxLayout>
#include <QSettings>

#include "globalobject.h"
#include "question.h"
#include "useranswer.h"
#include "mode.h"
#include "ticker.h"
#include "settings.h"
#include "questionform.h"

#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    void displayQuestion(int num);
    void startExam(int questionAmount);
    void startBrowsing();
    QList<UserAnswer*> userAnswers;
    QList<Question*> questions;
    int currentQuestion;
    void checkRanges();
    void uncheckAnswers();
    mode currentMode;
    mode previousMode;
    void renderQuestionNum();
    void computeResults();
    void updateButtons();
    void clearLayout(QLayout* layout, bool deleteWidgets);
    QList<QCheckBox*> checkboxes;
    QList<QLabel*> labels;
    Ticker* ticker;
    QSettings* preferences;
    bool validateExamRules();
private slots:
    void prevButtonPressed();
    void nextButtonPressed();
    void restartButtonPressed();
    void showAnswersButtonPressed();
    void checkboxValueChanged(int);
    void checkAnswer();
    void finishExam();
    void actionTraining();
    void actionExam();
    void actionSettingsDialogOpen();
    void actionQuestionFormOpen();
    void editQuestion();
    void deleteQuestion();
public slots:
    void updateCounter(QTime time);
};

#endif // MAINWINDOW_H
