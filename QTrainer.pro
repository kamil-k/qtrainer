QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTrainer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    globalobject.cpp \
    answer.cpp \
    question.cpp \
    useranswer.cpp \
    ticker.cpp \
    settings.cpp \
    questionform.cpp \
    category.cpp \
    certificate.cpp

HEADERS  += mainwindow.h \
    database.h \
    globalobject.h \
    answer.h \
    question.h \
    useranswer.h \
    mode.h \
    ticker.h \
    settings.h \
    questionform.h \
    category.h \
    certificate.h

FORMS    += mainwindow.ui \
    settings.ui \
    questionform.ui

RESOURCES += \
    resources.qrc

win32:RC_ICONS += resources\main.ico
win32:VERSION = 0.1
