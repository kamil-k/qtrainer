#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    preferences = new QSettings("config.cfg",QSettings::IniFormat);
    ui->timeEditExamTime->setTime(preferences->value("exam.time",QTime(1,40,0)).toTime());
    ui->spinBoxQuestionAmount->setValue(preferences->value("exam.question_amount", QVariant(10)).toInt());
    ui->checkBoxAmountOfAnswers->setChecked(preferences->value("exam.show_answers_amount",QVariant(false)).toBool());
    Database* database = globalObject.database;
    ui->comboBoxCertificationType->addItem("Select");
    ui->comboBoxCertificationType->addItems(database->toStringList(database->getAllCertificates()));
    int certTypeId = preferences->value("exam.certification_type", QVariant(0)).toInt();
    if(certTypeId==0){
        ui->comboBoxCertificationType->setCurrentIndex(0);
    }
    else if(database->amountOfQuestionsByCertificateId(certTypeId)==0){
        preferences->setValue("exam.certification_type", QVariant(0) );
         ui->comboBoxCertificationType->setCurrentIndex(0);
    }
    else{
        ui->comboBoxCertificationType->setCurrentText(    database->getCertificateNameById(certTypeId)      );
    }
    connect(ui->pushButtonOk,SIGNAL(clicked()),this,SLOT(ok()));
    connect(ui->pushButtonCancel,SIGNAL(clicked()),this,SLOT(cancel()));
}

Settings::~Settings()
{
    delete ui;
    delete preferences;
}

void Settings::ok(){
    Database* database = globalObject.database;
    preferences->setValue("exam.time",QVariant(ui->timeEditExamTime->time()));
    preferences->setValue("exam.question_amount",QVariant(ui->spinBoxQuestionAmount->value()));
    preferences->setValue("exam.show_answers_amount",QVariant(ui->checkBoxAmountOfAnswers->isChecked()));
    if(ui->comboBoxCertificationType->currentIndex()!=0){
        preferences->setValue("exam.certification_type", QVariant(database->getCertificateIdByName(ui->comboBoxCertificationType->currentText())).toInt());
    }
    else{
        preferences->setValue("exam.certification_type",QVariant(0));
    }
    this->close();
}

void Settings::cancel(){
    this->close();
}
