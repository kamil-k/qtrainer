#ifndef CATEGORY_H
#define CATEGORY_H

#include <QString>

class Category
{
public:
    Category();
    int categoryId;
    QString name;
};

#endif // CATEGORY_H
