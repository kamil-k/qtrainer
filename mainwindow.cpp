#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ticker = NULL;
    preferences = new QSettings("config.cfg",QSettings::IniFormat);
    ui->questionTextEdit->setStyleSheet("font: 14px;");
    ui->questionTextEdit->setEnabled(true);
    ui->questionTextEdit->setReadOnly(true);
    connect(ui->nextButton,SIGNAL(clicked()),this,SLOT(nextButtonPressed()));
    connect(ui->prevButton,SIGNAL(clicked()),this,SLOT(prevButtonPressed()));
    connect(ui->endButton,SIGNAL(clicked()),this,SLOT(finishExam()));
    connect(ui->answersButton,SIGNAL(clicked()),this,SLOT(showAnswersButtonPressed()));
    QActionGroup* group = new QActionGroup( this );
    ui->actionBrowse->setActionGroup(group);
    ui->actionExam->setActionGroup(group);
    ui->actionBrowse->setChecked(true);
    connect(ui->actionBrowse,SIGNAL(triggered()),this,SLOT(actionTraining()));
    connect(ui->actionExam,SIGNAL(triggered()),this,SLOT(actionExam()));
    connect(ui->actionSettings,SIGNAL(triggered()),this,SLOT(actionSettingsDialogOpen()));
    connect(ui->actionAdd_question,SIGNAL(triggered()),this,SLOT(actionQuestionFormOpen()));
    connect(ui->pushButtonEditQuestion,SIGNAL(clicked()),this,SLOT(editQuestion()));
    connect(ui->deleteButton,SIGNAL(clicked()),this,SLOT(deleteQuestion()));
    currentMode = NONE;
    actionTraining();
    connect(ui->restartButton,SIGNAL(clicked()),this,SLOT(restartButtonPressed()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete preferences;
}

void MainWindow::renderQuestionNum(){
    QString questionNumContent;
    if(questions.size()==0){
        questionNumContent = "0 / 0";
    }
    else{
        Question* question = questions.at(currentQuestion-1);
        questionNumContent = QString::number(currentQuestion)+ " / " + QString::number(questions.size());
        if(preferences->value("exam.show_answers_amount").toBool()){
            questionNumContent += " ( " + QString::number(question->correctAnswers) + " answer(s) ) ";
        }
    }
    ui->questionNumLabel->setText(questionNumContent);
}

void MainWindow::displayQuestion(int num){
    qDebug() << "Rendering question";
    Question* question = questions.at(--num);
    QString content = question->questionContent;
    if(currentMode==EXAM_RESULTS||currentMode==BROWSE_ANSWERS){
        QList<int> answerIds = globalObject.database->getAnswerIdsByQuestion(question->questionId);
        QList<int> mixedIds;
        for(int j=0; j<answerIds.size();j++){
            mixedIds.append(question->answers.at(j)->answerId);
        }
        QString exmplanationContent = question->explanation;
        for(int i=0; i<question->answers.size();i++){
            QString param = "%" + QString(QChar(65+i)) + "%";
            if(exmplanationContent.contains(param)){
                int answerOrder = mixedIds.indexOf(answerIds.at(i));
                QChar sign = 65+answerOrder;
                exmplanationContent.replace(param,"(" + QString(sign) + ")");
            }
        }
        content.append("\n\n EXPLANATION \n\n" + exmplanationContent);
    }
    ui->questionTextEdit->setPlainText(content);
    checkboxes.clear();
    labels.clear();
    this->clearLayout(ui->answersLayout, true);
    for(int i=0; i<question->answers.size();i++){
        QChar sign = (char)65+i;
        QHBoxLayout* answerLayout = new QHBoxLayout();
        QCheckBox* answerCheckbox = new QCheckBox();
        QLabel* answerLabel = new QLabel();
        checkboxes.append(answerCheckbox);
        labels.append(answerLabel);
        answerLabel->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);
        answerLabel->setText("(" + QString(sign) + ") " + question->answers.at(i)->answerContent);
        answerLayout->addWidget(answerCheckbox);
        answerLayout->addWidget(answerLabel);
        ui->answersLayout->addLayout(answerLayout);
        if(userAnswers.at(currentQuestion-1)->answers[i]){
            answerCheckbox->setChecked(true);
        }
        else{
            answerCheckbox->setChecked(false);
        }
        answerCheckbox->setProperty("QUESTION_NUM",QVariant(i));
        connect(answerCheckbox,SIGNAL(stateChanged(int)),this,SLOT(checkboxValueChanged(int)));
        if(currentMode==EXAM_RESULTS){
            answerCheckbox->setEnabled(false);
        }

    }
    checkAnswer();
}

void MainWindow::startBrowsing(){
    currentMode = BROWSE;
    questions = globalObject.database->getAllQuestions();
    userAnswers.clear();
    for(int i=0; i<questions.size(); i++){
        UserAnswer* userAnswer = new UserAnswer(questions.at(i)->answers.size());
        userAnswers.append(userAnswer);
    }
    currentQuestion = 1;
    if(questions.size()!=0){
        displayQuestion(currentQuestion);
    }
    checkRanges();
    renderQuestionNum();
}

void MainWindow::startExam(int questionAmount){
    currentMode = EXAM;
    int certificationId = preferences->value("exam.certification_type").toInt();
    questions = globalObject.database->getRandomQuestionsByCertificateId(questionAmount,certificationId);
    userAnswers.clear();
    if(ticker!=NULL&&ticker->isRunning()){
        qDebug() << "Killing ticker!";
        ticker->blockSignals(true);
        ticker->terminate();
        delete ticker;
    }
    ticker = new Ticker(preferences->value("exam.time").toTime());
    connect(ticker, SIGNAL(resultReady(QString)), this, SLOT(finishExam()));
    connect(ticker, SIGNAL(finished()), ticker, SLOT(deleteLater()));
    connect(ticker, SIGNAL(tick(QTime)), this, SLOT(updateCounter(QTime)));
    ticker->start();
    for(int i=0; i<questions.size(); i++){
        UserAnswer* userAnswer = new UserAnswer(questions.at(i)->answers.size());
        userAnswers.append(userAnswer);
    }
    currentQuestion = 1;
    displayQuestion(currentQuestion);
    checkRanges();
    renderQuestionNum();
}

void MainWindow::prevButtonPressed(){
    currentQuestion--;
    displayQuestion(currentQuestion);
    checkRanges();
    checkAnswer();
    renderQuestionNum();
}

void MainWindow::nextButtonPressed(){
    currentQuestion++;
    displayQuestion(currentQuestion);
    checkRanges();
    checkAnswer();
    renderQuestionNum();
}

void MainWindow::checkRanges(){
    if(questions.size()==0){
        ui->prevButton->setEnabled(false);
        ui->nextButton->setEnabled(false);
    }
    else{
        if(currentQuestion==1){
            ui->prevButton->setEnabled(false);
        }
        else{
            ui->prevButton->setEnabled(true);
        }
        if(currentQuestion==questions.size()){
            ui->nextButton->setEnabled(false);
        }
        else{
            ui->nextButton->setEnabled(true);
        }
    }
}

void MainWindow::checkboxValueChanged(int val){
    QCheckBox* checkbox = dynamic_cast<QCheckBox*>(sender());
    int answerNum = checkbox->property("QUESTION_NUM").toInt();
    if(val==Qt::Unchecked){
        userAnswers.at(currentQuestion-1)->answers[answerNum] = false;
    }
    else if(val==Qt::Checked){
        userAnswers.at(currentQuestion-1)->answers[answerNum] = true;
    }
    checkAnswer();
}

void MainWindow::checkAnswer(){
    qDebug() << "Checking answers answers";
    if(currentMode==EXAM||currentMode==BROWSE){
        for(int i=0;i<questions.at(currentQuestion-1)->answers.size();i++){
            qDebug() << "Painting answer to black";
            labels.at(i)->setStyleSheet("color: black; font: bold 14px;");
        }
    }
     if(currentMode==EXAM_RESULTS||currentMode==BROWSE_ANSWERS){
        for(int i=0;i<questions.at(currentQuestion-1)->answers.size();i++){
            qDebug() << "Checking answer of question: " + QString::number(i);
            qDebug() << QString("User answer: ") + QString::number(userAnswers.at(currentQuestion-1)->answers[i]);
            qDebug() << QString("Right answer: ") + QString::number(questions.at(currentQuestion-1)->answers.at(i)->correct);
            if(userAnswers.at(currentQuestion-1)->answers[i] == true && questions.at(currentQuestion-1)->answers.at(i)->correct == true){
                qDebug() << "Painting answer to green";
                labels.at(i)->setStyleSheet("color: green; font: bold 14px;");

            }
            else if(userAnswers.at(currentQuestion-1)->answers[i]!=questions.at(currentQuestion-1)->answers.at(i)->correct){
                qDebug() << "Painting answer to red";
                labels.at(i)->setStyleSheet("color: red; font: bold 14px;");
            }
            else{
                qDebug() << "Painting answer to black";
                labels.at(i)->setStyleSheet("color: black; font: bold 14px;");
            }
        }
     }
     this->repaint();

}

void MainWindow::computeResults(){
    int amountCorrect = 0;
    for(int i=0; i<questions.size(); i++){
        bool correct = true;
        for(int j=0;j<questions.at(i)->answers.size();j++){
            if(userAnswers.at(i)->answers[j]!=questions.at(i)->answers.at(j)->correct){
                correct = false;
                break;
            }
        }
        if(correct){
            amountCorrect++;
        }
    }
    int percent = 0;
    if(amountCorrect!=0){
        percent = (int)((amountCorrect / (float) questions.size() ) * 100);
    }
    ui->scoreLabel->setText(QString::number(amountCorrect) + " / " + QString::number(questions.size()) + " ( " + QString::number(percent) + "% )");
}

void MainWindow::uncheckAnswers(){
    qDebug() << "Unchecking answers";
    for(int i=0;i<questions.at(currentQuestion-1)->answers.size();i++){
        labels.at(i)->setStyleSheet("color: black; font: bold 14px;");
    }
}

void MainWindow::finishExam(){
    currentMode = EXAM_RESULTS;
    if(ticker->isRunning()){
        ticker->blockSignals(true);
        ticker->exit(0);
    }
    ui->endButton->setEnabled(false);
    for(int i=0;i<questions.at(currentQuestion-1)->answers.size();i++){
        checkboxes.at(i)->setEnabled(false);
    }
    displayQuestion(currentQuestion);
    checkAnswer();
    computeResults();
}

void MainWindow::clearLayout(QLayout* layout, bool deleteWidgets = true){
    while (QLayoutItem* item = layout->takeAt(0)){
        QWidget* widget;
        if (  (deleteWidgets)
              && (widget = item->widget())  ) {
            delete widget;
        }
        if (QLayout* childLayout = item->layout()) {
            clearLayout(childLayout, deleteWidgets);
        }
        delete item;
    }
}

void MainWindow::updateCounter(QTime time){
    ui->timeLabel->setText(time.toString());
}

void MainWindow::updateButtons(){
    if(questions.size()==0){
        ui->pushButtonEditQuestion->setEnabled(false);
        ui->answersButton->setEnabled(false);
        ui->deleteButton->setEnabled(false);
    }
    else{
        ui->pushButtonEditQuestion->setEnabled(true);
        ui->answersButton->setEnabled(true);
        ui->deleteButton->setEnabled(true);
    }
    ui->endButton->hide();
    ui->answersButton->hide();
    ui->restartButton->hide();
    ui->pushButtonEditQuestion->hide();
    ui->deleteButton->hide();
    if(currentMode==EXAM||currentMode==EXAM_RESULTS){
        ui->endButton->show();
        ui->restartButton->show();
    }
    else if(currentMode==BROWSE){
        ui->answersButton->show();
        ui->answersButton->setText("Show answers");
        ui->pushButtonEditQuestion->show();
        ui->deleteButton->show();
    }
    else if(currentMode==BROWSE_ANSWERS){
        ui->answersButton->show();
        ui->answersButton->setText("Hide answers");
        ui->pushButtonEditQuestion->show();
        ui->deleteButton->show();
    }
}

void MainWindow::showAnswersButtonPressed(){
    if(currentMode==BROWSE){
        currentMode=BROWSE_ANSWERS;
        displayQuestion(currentQuestion);
    }
    else{
        currentMode=BROWSE;
        displayQuestion(currentQuestion);
    }
    updateButtons();
}

void MainWindow::actionTraining(){
    if(currentMode!=BROWSE&&currentMode!=BROWSE_ANSWERS){
        this->startBrowsing();
        updateButtons();
        ui->timeDescLabel->hide();
        ui->timeLabel->hide();
        ui->scoreDescLabel->hide();
        ui->scoreLabel->hide();
    }
}

void MainWindow::actionExam(){
    if(!validateExamRules()){
        if((previousMode==EXAM||previousMode==EXAM_RESULTS)&&currentMode==NONE){
            ui->actionExam->setChecked(true);
            currentMode=previousMode;
        }
        return;
    }
    if(currentMode!=EXAM&&currentMode!=EXAM_RESULTS){
        int confQuestionAmount = preferences->value("exam.question_amount").toInt();
        this->startExam(confQuestionAmount);
        ui->endButton->setEnabled(true);
        updateButtons();
        ui->scoreLabel->setText("Unknown");
        ui->timeDescLabel->show();
        ui->timeLabel->show();
        ui->scoreDescLabel->show();
        ui->scoreLabel->show();
    }
}

void MainWindow::restartButtonPressed(){
    previousMode = currentMode;
    currentMode = NONE;
    actionExam();
}

void MainWindow::actionSettingsDialogOpen(){
    Settings* settings = new Settings(this);
    settings->exec();
    delete settings;
    renderQuestionNum();

}

void MainWindow::actionQuestionFormOpen(){
    if(currentMode==EXAM||currentMode==EXAM_RESULTS){
        QMessageBox::critical(this,"Error","Qustion management is not possible in exam mode. Please switch to training");
        return;
    }
    QuestionForm* questionForm = new QuestionForm(0,this);
    questionForm->exec();
    delete questionForm;
    if(currentMode==BROWSE||currentMode==BROWSE_ANSWERS){
        questions = globalObject.database->getAllQuestions();
        userAnswers.clear();
        for(int i=0; i<questions.size(); i++){
            UserAnswer* userAnswer = new UserAnswer(questions.at(i)->answers.size());
            userAnswers.append(userAnswer);
        }
        if(questions.size()!=0){
            displayQuestion(currentQuestion);
        }
         renderQuestionNum();
        updateButtons();
        checkRanges();
    }
}

void MainWindow::editQuestion(){
    if(currentMode==EXAM||currentMode==EXAM_RESULTS){
        QMessageBox::critical(this,"Error","Qustion management is not possible in exam mode. Please switch to training");
        return;
    }
    QuestionForm* questionForm = new QuestionForm(questions.at(currentQuestion-1)->questionId,this);
    questionForm->exec();
    delete questionForm;
    if(currentMode==BROWSE||currentMode==BROWSE_ANSWERS){
        questions = globalObject.database->getAllQuestions();
        userAnswers.clear();
        for(int i=0; i<questions.size(); i++){
            UserAnswer* userAnswer = new UserAnswer(questions.at(i)->answers.size());
            userAnswers.append(userAnswer);
        }
        displayQuestion(currentQuestion);
        renderQuestionNum();
        updateButtons();
        checkRanges();
    }
}

bool MainWindow::validateExamRules(){
    bool result = true;
    int certificationId = preferences->value("exam.certification_type").toInt();
    if(certificationId==0){
        ui->actionBrowse->setChecked(true);
        QMessageBox::critical(this,"Error","Certification type is not selected");
        return false;
    }
    int confQuestionAmount = preferences->value("exam.question_amount").toInt();
    int quetionAmount = globalObject.database->amountOfQuestionsByCertificateId(certificationId);
    if(questions.size()==0){
        QMessageBox::critical(this,"Error","No questions in database, start the exam is not possible.");
        ui->actionBrowse->setChecked(true);
        result = false;
    }
    else if(confQuestionAmount>quetionAmount){
        QMessageBox::critical(this,"Error","Amount of exam questions in selected certification type is grather than amount of quesitons in database, add new questions or change exam configuration.");
        ui->actionBrowse->setChecked(true);
        result = false;
    }
    return result;
}

void MainWindow::deleteQuestion(){
    globalObject.database->deleteQuestionById(questions.at(currentQuestion-1)->questionId);
    if(currentQuestion!=1){
        currentQuestion--;
    }
    if(currentMode==BROWSE||currentMode==BROWSE_ANSWERS){
        questions = globalObject.database->getAllQuestions();
        userAnswers.clear();
        for(int i=0; i<questions.size(); i++){
            UserAnswer* userAnswer = new UserAnswer(questions.at(i)->answers.size());
            userAnswers.append(userAnswer);
        }

        if(questions.size()!=0){
            displayQuestion(currentQuestion);
        }
        else{
            ui->questionTextEdit->setPlainText("");
            checkboxes.clear();
            labels.clear();
            this->clearLayout(ui->answersLayout, true);
        }
        renderQuestionNum();
        updateButtons();
        checkRanges();
    }
}
